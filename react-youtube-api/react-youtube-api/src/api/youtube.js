import axios from 'axios';
const KEY = 'AIzaSyD8YgmEFE_i9hvHiYTQAeDm2vVRLVNjEqs';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3/',
    params: {
        part: 'snippet',
        maxResult: 5,
        key: KEY
    }
})