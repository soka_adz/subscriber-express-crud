var numSquares = 6;
var colors = [];
var pickedColor;
var squares = document.querySelectorAll(".square");
var resetButton = document.querySelector("#reset");
var modeButtons = document.querySelectorAll(".mode");
var messageDisplay = document.querySelector("#message");
var display = document.querySelector('#colorDisplay');

var colors = ["rgb(255, 0, 0)", "rgb(255, 0, 255)", "rgb(255, 225, 0)", 
"rgb(255, 0, 255)", "rgb(0, 255, 255)", "rgb(0, 255, 0)"];

init();

function init(){
	setupModeButtons();
	setupSquares();
	reset();
}

function setupModeButtons(){
	for(var i = 0; i < modeButtons.length; i++){
		modeButtons[i].addEventListener("click", function(){
			modeButtons[0].classList.remove("selected");
			modeButtons[1].classList.remove("selected");
			this.classList.add("selected");
			this.textContent === "Easy" ? numSquares = 3: numSquares = 6;
			reset();
		});
	}
}

// display warna
messageDisplay.textContent= pickedColor;
console.log(display);


for (i = 0; i < squares.length; i++) {
    squares[i].style.backgroundColor = colors[i];
    
}

function changeColors(color) {
    for (i = 0; i <= squares.length; i++) {
        squares[i].style.backgroundColor = color;
        messageDisplay.text = "You are good at guessing!";
    }
}

function pickColor() {
    var random = Math.floor(Math.random()* colors.length)
    return colors[random]
}

// var pickedColor = pickedColor()
function changeColors(color) {
    for (i = 0; i < squares.length; i++) {
        squares[i].style.backgroundColor = color;
    }
}

var pickedColor = pickColor();
display.textContent= pickedColor;
// console.log(pickedColor)
for (i = 0; i < squares.length; i++) {
    //apply background colour to all the squares
    //enable click event on each square.....     
    squares[i].addEventListener('click', function () {
        //if the user selected the right colour....         
        var clickedColor = this.style.backgroundColor;
        console.log(clickedColor)
        //check if the selected colour matches the default colour...
        if (pickedColor === clickedColor) {
            changeColors(pickedColor);
        } else {
            this.style.backgroundColor = "#232323";
            messageDisplay.text = "Wrong Choice!";
        }
    });
}
console.log(pickedColor)

function setupSquares(){
	for(var i = 0; i < squares.length; i++){
	//add click listeners to squares
		squares[i].addEventListener("click", function(){
			//grab color of clicked square
			var clickedColor = this.style.background;
			//compare color to pickedColor
			if(clickedColor === pickedColor){
				messageDisplay.textContent = "Correct!";
				resetButton.textContent = "Play Again?"
				changeColors(clickedColor);
				h1.style.background = clickedColor;
			} else {
				this.style.background = "#232323";
				messageDisplay.textContent = "Try Again"
			}
		});
	}
}

function reset(){
	colors = generateRandomColors(numSquares);
	//pick a new random color from array
	pickedColor = pickColor();
	//change colorDisplay to match picked Color
	colorDisplay.textContent = pickedColor;
	resetButton.textContent = "New Colors"
	messageDisplay.textContent = "";
	//change colors of squares
	for(var i = 0; i < squares.length; i++){
		if(colors[i]){
			squares[i].style.display = "block"
			squares[i].style.background = colors[i];
		} else {
			squares[i].style.display = "none";
		}
	}
	h1.style.background = "steelblue";
}

resetButton.addEventListener("click", function(){
	reset();
})

// function changeColors(pickedColor) {
//     for (i = 0; i <= squares.length; i++) {
//         squares[i].style.backgroundColor = pickedColor;
//         messageDisplay.text = "You are good at guessing!";
//     }
// }

function changeColors(color){
	//loop through all squares
	for(var i = 0; i < squares.length; i++){
		//change each color to match given color
		squares[i].style.background = color;
	}
}

function generateRandomColors(num){
	//make an array
	var arr = []
	//repeat num times
	for(var i = 0; i < num; i++){
		//get random color and push into arr
		arr.push(randomColor())
	}
	//return that array
	return arr;
}

function randomColor(){
	//pick a "red" from 0 - 255
	var r = Math.floor(Math.random() * 256);
	//pick a "green" from  0 -255
	var g = Math.floor(Math.random() * 256);
	//pick a "blue" from  0 -255
	var b = Math.floor(Math.random() * 256);
	return "rgb(" + r + ", " + g + ", " + b + ")";
}


// squares[0].style.backgroundColor = colors[0];
// console.log(colors)