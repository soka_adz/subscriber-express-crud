import axios from "axios";
import setAuthToken from "../utils/setAuthToken";
import jwt_decode from "jwt-decode";

import * as actionTypes from "./actionTypes";

// Register User
export const registerUser = (userData, history) => dispatch => {
    axios.post("/user", userData)
        .then(res => history.push("/login"))
        .catch(err => 
            dispatch({
                type: actionTypes.GET_ERRORS,
                payload: err.response.data
            }))
}

// Login - get user token
export const loginUser = userData => dispatch => {
    axios.post("/user/login", userData)
    .then(res => {
        const { token } = res.data;
        // Set token to localstorage
        localStorage.setItem("jwtToken", token);
        // Set token to Auth header
        setAuthToken(token);
        const decoded = jwt_decode(token);
        // Set current user
        dispatch(setCurrentUser(decoded));
    })
    .catch(err => dispatch({
        type: actionTypes.GET_ERRORS,
        payload: err.response.data
    }));
}

// Set logged in user
export const setCurrentUser = decoded => {
    return {
        type: actionTypes.SET_CURRENT_USER,
        payload: decoded
    }
}

// User loading
export const setUserLoading= () => {
    return {
        type: actionTypes.USER_LOADING
    }
}

// User log out
export const logoutUser = () => dispatch => {
    localStorage.removeItem("jwtToken")
    setAuthToken(false);
    dispatch(setCurrentUser({}));
    
}